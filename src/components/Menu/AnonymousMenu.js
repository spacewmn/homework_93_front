import React from 'react';
import {Link} from "react-router-dom";

const AnonymousMenu = () => {
    return (
        <>
            <Link className="nav-link ml-auto" to="/register">Sign Up</Link>
            <Link className="nav-link" to="/login">Sign In</Link>
        </>
    );
};

export default AnonymousMenu;