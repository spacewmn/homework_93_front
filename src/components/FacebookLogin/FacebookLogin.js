import React from 'react';
import FacebookLoginButtom from 'react-facebook-login/dist/facebook-login-render-props';
import {fbAppId} from "../../constants";
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../store/actions/userActions";

const FacebookLogin = () => {
    const dispatch = useDispatch();
    const facebookResponse = response => {
        if (response.id) {
            dispatch(facebookLogin(response));
        }
    };

    return <FacebookLoginButtom
        appId={fbAppId}
        fields="name,email,picture"
        render={renderProps => {
            return <button type="submit" className="btn btn-info mr-3" onClick={renderProps.onClick}>
                Enter with facebook
            </button>

        }}
        callback = {facebookResponse}
    />;
};

export default FacebookLogin;