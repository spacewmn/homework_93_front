import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props} /> :
        <Redirect to={redirectTo} />;
};

const Routes = ({user}) => {
    return (
        <Switch>
            <ProtectedRoute
                path="/register"
                exact
                component={Register}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path="/login"
                exact
                component={Login}
                isAllowed={!user}
                redirectTo="/"
            />
        </Switch>
    );
};

export default Routes;