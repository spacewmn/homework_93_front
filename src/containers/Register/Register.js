import React, {useState} from 'react';
import FormElement from "../../components/FormElement/FormElement";
import {NavLink} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {registerUser} from "../../store/actions/userActions";

const Register = () => {

    const [state, setState] = useState({
        username: "",
        email: "",
        password: ""
    });

    console.log("yes");

    const error = useSelector(state => state.users.registerError)
    const dispatch = useDispatch();

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    }

    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...state}));
    };

    return (
        <div>
            <h1>Sign Up</h1>
            <form className="my-5" onSubmit={formSubmitHandler} >
                <FormElement
                    name='username'
                    label="Username"
                    type='text'
                    helperText={getFieldError('username')}
                    value={state.username}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    name='email'
                    type= 'email'
                    label="E-mail"
                    value={state.email}
                    helperText={getFieldError('email')}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    name='password'
                    type='password'
                    label="Password"
                    helperText={getFieldError('password')}
                    value={state.password}
                    onChange={inputChangeHandler}
                />
                <button type="submit" className="btn btn-primary">Sign Up</button>
                <div className="my-4">
                    <NavLink to="/login">Already have an account? Sign in</NavLink>
                </div>
            </form>
        </div>
    );
}
export default Register;